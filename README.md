# Hoja03_Markdown_02 

### PASOS A SEGUIR

1. Crea un repositorio en GitLab o GitHub llamado TUNOMBRE_markdown  
	![](imagenes/imagen1.jpg "Repositorio GitLab")  

2. Clona el repositorio en local   
	* Entramos en la carpeta que va a contener el repositorio, lo iniciamos mediante git init y clonamos el repositorio remoto.
	![](imagenes/imagen2.jpg "Git init - Git clone")  

3. Crea en tu repositorio local un documento README.md Nota: en este documento tendrás que ir poniendo los comandos que has tenido que utilizar durante todos los ejercicios y las explicaciones y capturas de pantalla que consideres necesarias. 
	![](imagenes/imagen3.jpg "touch README.md")  

4. Añadir al README.md los comandos utilizados hasta ahora y hacer un commit inicial con el mensaje “Primer commit de TUNOMBRE”. 
	![](imagenes/imagen4.jpg "Pasos archivo readme")  

5. Sube los cambios al repositorio remoto.
	* git add .
	* git commit -m "Primer commit de Paula"
	* git push origin master
	![](imagenes/imagen5.jpg "ADD - COMMIT - PUSH")  
	
6. Crear en el repositorio local un fichero llamado privado.txt. Crear en el repositorio local una carpeta llamada privada.
	* Creamos la carpeta en el repositorio local y archivo privado.txt
	![](imagenes/imagen6.jpg "Touch privado.txt")  

7. Realizar los cambios oportunos para que tanto el archivo como la carpeta sean ignorados por git. 
	* Creamos el archivo .gitignore y le indicamos los archivos que queremos que ignore.
	![](imagenes/imagen7.jpg ".gitignore")
	* Subimos los cambios al repositorio remoto mediante: git add . - git commit - git push origin master
	![](imagenes/imagen8.jpg "Cambios")

8. Documenta los puntos e) f) y g) en el fichero README.md  
	![](imagenes/imagen9.jpg "Documentación")

9. Añade el fichero tunombre.md en el que se muestre un listado de los módulos en los que estás matriculado.  
	![](imagenes/imagen10.jpg "Fichero paula.md")

10. Crea un tag llamado v0.1  
	![](imagenes/imagen11.jpg "git tag v0.1")

11. Sube los cambios al repositorio remoto  
	![](imagenes/imagen12.jpg "Cambios")

12. Documenta los puntos i) j) y k) en el README.md  
	![](imagenes/imagen13.jpg "Documentación")

13. Por último, crea una tabla en el documento anterior en el que se muestre el nombre de 2 compañeros y su enlace al repositorio en GitLab o GitHub.

|    Nombre    |				 Repositorio 					 |
|:------------:|:-----------------------------------------------:|
|Carlos  	   |https://gitlab.com/Carlosangel17/carlos_markdown |
|Karla		   |https://gitlab.com/KarlaNC/karla_markdown        |

###### [Repositorio GitLab](https://gitlab.com/PRGGR/paula_markdown  "Repositorio GitLab")  
![](imagenes/imagen14.jpg "Repositorio gitlab")


***
***

# Hoja03_Markdown_03 

## A partir de la tarea anterior vamos a profundizar más en el uso de Git:

### PASOS A SEGUIR

1. Creación de ramas:
	* Crea la rama rama-TUNOMBRE  y posiciona tu carpeta de trabajo en esta rama
	![](imagenes02/imagen1.jpg "git checkout -b paula")

2. Añade un fichero y crea la rama remota
	* Crea un fichero llamado daw.md con únicamente una cabecera DESARROLLO DE APLICACIONES WEB
	![](imagenes02/imagen2.jpg "archivo daw.md")
	* Haz un commit con el mensaje “Añadiendo el archivo daw.md en la rama-TUNOMBRE” y sube los cambios al repositorio remoto.
	NOTA: date cuenta que ahora se deberá hacer con el comando git push origin rama-TUNOMBRE
	![](imagenes02/imagen3.jpg "Subir archivo a rama Paula")

3. Haz un merge directo
	* Posiciónate en la rama master  
	![](imagenes02/imagen4.jpg "git checkout master")
	* Haz un merge de la rama-TUNOMBRE en la rama master  
	![](imagenes02/imagen5.jpg "git merge paula")

4. Haz un merge con conflicto
	* En la rama master añade al fichero daw.md una tabla en la que muestres los módulos del primer curso de DAW.  
	![](imagenes02/imagen6.jpg "Modificar daw.md")
	* Añade los archivos y haz un commit con el mensaje “Añadida tabla DAW1”  
	![](imagenes02/imagen7.jpg "Commit rama Master")
	* Posiciónate ahora en la rama-TUNOMBRE  
	![](imagenes02/imagen8.jpg "Git checkout paula")
	* Escribe en el fichero daw.md otra tabla con los módulos del segundo curso de DAW  
	![](imagenes02/imagen9.jpg "Modificar daw.md")
	* Añade los archivos y haz un commit con el mensaje “Añadida tabla DAW2”  
	![](imagenes02/imagen10.jpg "Modificar daw.md")
	* Posiciónate otra vez en master y haz un merge con la rama-TUNOMBRE  
	![](imagenes02/imagen11.jpg "Checkout y merge - CONFLICTO")

5. Arreglo del conflicto
	* Arregla el conflicto editando el fichero daw.md y haz un commit con el mensaje “Finalizado el conflicto de daw.md”
		* Conflicto dentro del archivo en sublime  
		![](imagenes02/imagen12.jpg "Conflicto archivo daw.md")
		* Arreglar a mano los fallos  
		![](imagenes02/imagen13.jpg "Corrección archivo daw.md")
		* Subimos el archivo de nuevo y hacemos commit  
		![](imagenes02/imagen14.jpg "Add + commit + Push")

6. Tag y borrar la rama
	* Crea un tag llamado v0.2  
	![](imagenes02/imagen15.jpg "Git tag v0.2")
	* Borra la rama-TUNOMBRE  
	![](imagenes02/imagen16.jpg "Git branch -d paula")

7. Documenta todo y finaliza el ejercicio
	* En el fichero README.md crea una nueva sección en la que vayas documentando todo lo que vas realizando en esta tarea.
	* Documenta todos los puntos en el README.md, haz un commit y sube los cambios al servidor
	* Haz un último commit y sube todo al servidor